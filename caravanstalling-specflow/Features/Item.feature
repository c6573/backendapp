﻿Feature: Item

@AddItemForCustomerWithId1
Scenario: Add item for customer with id 1
	Given we want to add an item for customer with id 1
	And the item type is Auto
	And the item lenght is 6
	And the licenceplate number is AA-BB-CC
	When we add the item
	Then the item response should be 200

@AddItemForCustomerWithId1WhereTypeIsEmpty
Scenario: Add item for customer with id 1 where type is empty
	Given we want to add an item for customer with id 1
	And the item type is empty
	And the item lenght is 6
	And the licenceplate number is AA-BB-CC
	When we add the item
	Then the item response should be 400

@AddItemForCustomerWithId1WhereLengthIsEmpty
Scenario: Add item for customer with id where lenght is empty
	Given we want to add an item for customer with id 1
	And the item type is Auto
	And the item lenght is 0
	And the licenceplate number is AA-BB-CC
	When we add the item
	Then the item response should be 400

@AddItemForCustomerWithId1WhereThereIsNoOwner
Scenario: Add item with no owner
	Given we want to add an item for with no owner
	And the item type is Auto
	And the item lenght is 6
	And the licenceplate number is AA-BB-CC
	When we add the item
	Then the item response should be 400

@AddItemForCustomerWithId1WhereTypeDoesNotExist
Scenario: Add item for customer with id 2 where type does not exist
	Given we want to add an item for customer with id 2
	And the item type is Vliegtuig
	And the item lenght is 6
	And the licenceplate number is AA-BB-CC
	When we add the item
	Then the item response should be 400

@UpdateItemWithId1
Scenario: Update item with id 2
	Given we want to update item with id 2
	And we set the type to Camper
	And we set the length to 4
	When we update the item
	Then the item response should be 200

@UpdateItemWithId1WhereTypeIsEmpty
Scenario: Update item with id 2 where type is empty
	Given we want to update item with id 2
	And we set the type to empty
	And we set the length to 4
	When we update the item
	Then the item response should be 400

@UpdateItemWithId1WhereLengthIsEmpty
Scenario: Update item with id 2 where lenght is empty
	Given we want to update item with id 2
	And we set the type to Camper
	And we set the length to 0
	When we update the item
	Then the item response should be 400

@UpdateItemWithId1WhereTypeDoesNotExist
Scenario: Update item with id 2 where type does not exist
	Given we want to update item with id 2
	And we set the type to Vliegtuig
	And we set the length to 6
	When we update the item
	Then the item response should be 400

@GetItemWithId1
Scenario: get item with item id 2
	Given we want to retrieve the item with id 2
	When we retrieve the item
	Then the item response should be 200

@GetItemWithIdThatDoesNotExist
Scenario: get item that does not exist
	Given we want to retrieve the item with id 10000000
	When we retrieve the item
	Then the item response should be 404

@DeleteAnItem
Scenario: delete an item
	Given we want to delete a item
	When we delete this item
	Then the action response should be 200

@DeleteAnItemThatDoesNotExist
Scenario: delete an item that does not exist
	Given we want to delete a item with id 1000000
	When we delete this item
	Then the action response should be 404

@DeleteThatStillHasSecondaryOwners
Scenario: delete an item that still has secondary owners
	Given we want to delete a item with id 2
	When we delete this item
	Then the action response should be 400
