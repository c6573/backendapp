﻿Feature: Document

@AddDocumentForCustomer
Scenario: Add document file for customer with id 1
	Given the customer id for the document is 1
	And the document to save is called testPdf.pdf
	When the document is saved
	Then the list of documents should be + 1 

@AddDocumentForItem
Scenario: Add document file for item with id 2
	Given the item id for the document is 2
	And the document to save is called testPdf.pdf
	When the document is saved
	Then the list of documents should be + 1

@DownloadfileShouldSucceed
Scenario: Download a file under normal conditions
	Given the customer id for the document is 2
	When we try to download a document
	Then the response should be 200

@DownloadfileShouldFail
Scenario: Download a file that does not exist
	Given we try to dowload a document with id 1000000
	When we download the document with that id
	Then the response should be 400

@DeletedFirstDocumentInListForCustomer
Scenario: Delete the first document of a customer
	Given the customer id for the document is 2
	When the first document is deleted
	Then the list of documents should be - 1

@AddDocumentWithoutFileExtension
Scenario: Add a document withoud a file extension
	Given the customer id for the document is 1
	And the document to save is called testPdf
	When the document is saved
	Then the response should be 400

	
@AddDocumentWithoutFileSize
Scenario: Add a document with 0kb
	Given the customer id for the document is 1
	And the document to save is called testPdf.pdf
	And the size of the document is null
	When the document is saved
	Then the response should be 400

@DeleteFileThatDoesNotExist
Scenario: Try to delete a file that does not exist
	Given the customer id for the document is 1
	And the document we try to delete has id 1000000000
	When the document is deleted
	Then the response should be 400
