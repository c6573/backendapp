﻿Feature: Person

A short summary of the feature

@AddNewCustomer
Scenario: Add a new customer
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is +31612345678
	And the customers unique email is testemail@test.nl
	When we save this customer 
	Then the customer response code should be 200

@UpdateCustomer
Scenario: Update a customer
	Given we want to update the customer with id 1
	When we set his firstname to test customer and save
	Then the customer response code should be 200

@GetCustomerById
Scenario: get customer by id
	Given we want to find customer with id 1
	When we retrieve the customer
	Then the customer response code should be 200

@GetCustomerThatDoesNotExist
Scenario: get a customer that does not exist
	Given we want to find customer with id 100000
	When we retrieve the customer
	Then the customer response code should be 404

@AddNewCustomerWithEmptyRequiredFieldfirstname
Scenario: Add a new customer with emtpy requirement field firstname
	Given We want to add a new customer with firstname empty
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is 06-12345678
	And the customers email is testemail@test.nl
	When we save this customer
	Then the customer response code should be 400

@AddNewCustomerWithEmptyRequiredFieldfirstname
Scenario: Add a new customer with emtpy requirement field lastname
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is empty
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is 06-12345678
	And the customers email is testemail@test.nl
	When we save this customer
	Then the customer response code should be 400

@AddNewCustomerWithEmptyRequiredFieldfirstname
Scenario: Add a new customer with emtpy requirement field phone number
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is empty
	And the customers email is testemail@test.nl
	When we save this customer
	Then the customer response code should be 400

@AddNewCustomerWithEmptyRequiredFieldfirstname
Scenario: Add a new customer with emtpy requirement field email
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is 06-12345678
	And the customers email is empty
	When we save this customer
	Then the customer response code should be 400

@AddNewCustomerThatAlreadyExists
Scenario: Add new customer that already exists
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is 06-12345678
	And the customers email is Hans@gmail.com
	When we save this customer
	Then the customer response code should be 400

@AddNewCustomerWithFaultyEmail
Scenario: Add new customer with a faulty email
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is 06-12345678
	And the customers email is ditisgeenemail
	When we save this customer
	Then the customer response code should be 400

@AddNewCustomerWithFaultyPhoneNumber
Scenario: Add new customer with a faulty phone number
	Given We want to add a new customer with firstname test voornaam
	And the customers last name is test achternaam
	And the customers lives on test straat : 1, 1234AB, Utrecht
	And the customers mobile phone number is 0a0a0a0a0
	And the customers email is Hans@gmail.com
	When we save this customer
	Then the customer response code should be 400

@UpdateCustomerWithoutId
Scenario: Update a customer without customerId
	Given we want to update the customer with id 1
	When we set the id of the customer to 0
	Then the customer response code should be 404

@UpdateCustomerWithoutFirstName
Scenario: Update a customer without Firstname
	Given we want to update the customer with id 1
	When we set the firstname of the customer to empty
	Then the customer response code should be 400

@UpdateCustomerWithoutLastName
Scenario: Update a customer without LastName
	Given we want to update the customer with id 1
	When we set the lastname of the customer to empty
	Then the customer response code should be 400

@UpdateCustomerWithoutEmail
Scenario: Update a customer without Email
	Given we want to update the customer with id 1
	When we set the email of the customer to empty
	Then the customer response code should be 400

@UpdateCustomerWithoutPhoneNumber
Scenario: Update a customer without Phonenumer
	Given we want to update the customer with id 1
	When we set the phonenumber of the customer to empty
	Then the customer response code should be 400

@UpdateCustomerWithFaultyEmail
Scenario: Update a customer with faulty email
	Given we want to update the customer with id 1
	When we set the email of the customer to ditisgeenemail
	Then the customer response code should be 400

@UpdateCustomerWithFaultyEmail
Scenario: Update a customer with faulty phoneNumber
	Given we want to update the customer with id 1
	When we set the email of the customer to 0a0a0a0a0
	Then the customer response code should be 400