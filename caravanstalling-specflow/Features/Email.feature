﻿Feature: Email


@ScheduleEmailforCustomer
Scenario: Schedule an email for customer with id 1
	Given the customer id for the scheduled email is 1
	And we want to schedule a email with type invoice
	When the email is scheduled
	Then the list of scheduled emails should be + 1 
	And the added scheduled email should have status pending
	And the amount reminded should be 0

@ScheduleEmailforCustomerWithoutId
Scenario: Schedule an email for customer with no id
	Given the customer id for the scheduled email is 0
	And we want to schedule a email with type invoice
	When the email is scheduled
	Then the email response should be 400

@ScheduleEmailforCustomerWithoutEmailType
Scenario: Schedule an email for customer without email type
	Given the customer id for the scheduled email is 1
	And we want to schedule a email with type doesNotExist
	When the email is scheduled
	Then the email response should be 400


@ScheduleEmailforCustomerWithInvalidEmail
Scenario: Schedule an email for customer with an invalid email adress
	Given the customer id for the scheduled email is 4
	And we want to schedule a email with type invoice
	When the email is scheduled
	Then the email response should be 400

@UpdateInvoiceTemplate
Scenario: Update the invoice template
	Given the template type is invoice
	And the new html string is <div> test </div>
	And the new json is {"test":"test", "another test": "test again"}
	When the template is saved
	Then the emailTemplate response should be 200

@UpdateInvoiceTemplatewithFaultyHtml
Scenario: Update the invoice template with faulty html string
	Given the template type is invoice
	And the new html string is div test div
	And the new json is {"test":"test", "another test": "test again"}
	When the template is saved
	Then the emailTemplate response should be 400

@UpdateInvoiceTemplateWithFaultyJson
Scenario: Update the invoice template with faulty json string
	Given the template type is invoice
	And the new html string is <div> test </div>
	And the new json is dit is geen json
	When the template is saved
	Then the emailTemplate response should be 400
