﻿Feature: Note

@AddNoteForCustomer
Scenario: Add note for customer with id 1
	Given the customer id is 1
	And the note to safe is test note for customer
	When the note is saved
	Then the result should be test note for customer

@AddNoteForItem
Scenario: Add note for item with id 2
	Given the item id is 2
	And the note to safe is test note for item
	When the note is saved
	Then the result should be test note for item

@SQLInjectionAttempt
Scenario: Add note for customer with id 2 with sql in note string, the application should not break
	Given the customer id is 2
	And the note to safe is DROP TABLE notes;
	When the note is saved
	Then the result should be DROP TABLE notes;


