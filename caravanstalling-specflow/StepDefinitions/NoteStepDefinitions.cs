﻿using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.DtoModels;

namespace caravanstalling_specflow.StepDefinitions
{
    [Binding]
    public sealed class NoteStepDefinitions
    {
        private NoteDto noteDto;
        private readonly INoteController notesController;
        private NoteDto noteResult;

        public NoteStepDefinitions(INoteController notesController) {
            this.notesController = notesController;
            this.noteDto = new NoteDto();
            this.noteResult = new NoteDto();
        }

        [Given("the customer id is (.*)")]
        public void GivenTheCustomerIdIs(int customerId)
        {
            noteDto.customerId = customerId;
        }

        [Given("the item id is (.*)")]
        public void GivenTheItemIdIs(int itemId)
        {
            noteDto.itemId = itemId;
        }

        [Given("the note to safe is (.*)")]
        public void TheNoteToSafeIs(String note)
        {
            noteDto.note = note;
        }

        [When("the note is saved")]
        public void WhenTheNoteIsSaved()
        {
            this.noteResult = this.notesController.saveNote(noteDto);
        }

        [Then("the result should be (.*)")]
        public void ThenTheResultShouldBe(string result)
        {
            this.noteResult.note.Should().Be(result);
        }
    }
}
