﻿using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.DtoModels;
using Caravanstalling_WebAPI.Models;
using Caravanstalling_WebAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace caravanstallingSpecflow.StepDefinitions
{
    [Binding]
    public sealed class DocumentStepDefinitions
    {
        private DocumentDto documentDto;
        private readonly IDocumentController documentController;
        private readonly IDocumentRepository documentRepository;
        private List<Document> documentsBeforeSave;
        private List<Document> documentsAfterSave;
        private List<Document> documentsAfterDelete;
        private ActionResult<DocumentDto> actionResult;
        private int documentToDeleteId;
        private int documentToDownload;

        public DocumentStepDefinitions(IDocumentController documentController, IDocumentRepository documentRepository) 
        {
            this.documentController = documentController;
            this.documentRepository = documentRepository;
            this.documentDto = new DocumentDto();
            this.documentsBeforeSave = new List<Document>();
            this.documentsAfterSave = new List<Document>();
            this.documentsAfterDelete = new List<Document>();
        }

        [Given("the customer id for the document is (.*)")]
        public void GivenTheCustomerIdIs(int customerId)
        {
            this.documentDto.customersId = customerId;
            this.documentsBeforeSave = this.documentRepository.getAllDocumentsForCustomer(customerId).Result;
        }

        [Given("the item id for the document is (.*)")]
        public void GivenTheItemIdIs(int itemId)
        {
            this.documentDto.itemId = itemId;
            this.documentsBeforeSave = this.documentRepository.getAllDocumentsForItem(itemId).Result;
        }

        [Given("the document to save is called (.*)")]
        public void GivenTheDocumentToSaveIsCalledTestPdf_Pdf(string fileName)
        {
            this.documentDto.fileName = fileName;
            this.documentDto.size = "2.3MB";
        }

        [When("the document is saved")]
        public void WhenTheDocumentIsSaved()
        {
            this.documentDto.fileString = File.ReadAllText(@"..\..\..\..\caravanstalling-specflow\TestResources\upload_document_base_64_string1.txt");
            this.actionResult = this.documentController.uploadFile(this.documentDto).Result;
        }

        [Then(@"the list of documents should be \+ (.*)")]
        public void ThenTheListOfDocumentsShouldBe(int extraDocument)
        {
            if (this.documentDto.customersId != null)
            {
                this.documentsAfterSave = this.documentRepository.getAllDocumentsForCustomer(this.documentDto.customersId).Result;
            }
            else 
            {
                this.documentsAfterSave = this.documentRepository.getAllDocumentsForItem(this.documentDto.itemId).Result;
            }
            this.documentsAfterSave.Count.Should().Be(this.documentsBeforeSave.Count + extraDocument);
        }

        [When("the first document is deleted")]
        public void WhenTheFirstDocumentIsDeleted()
        {
            int documentId = this.documentsBeforeSave.First().Id;
            var result = this.documentController.deleteDocument(documentId).Result;
        }

        [Then(@"the list of documents should be - (.*)")]
        public void ThenTheListOfDocumentsShouldBe_(int minusDocument)
        {
            this.documentsAfterDelete = this.documentRepository.getAllDocumentsForCustomer(this.documentDto.customersId).Result;
            this.documentsAfterDelete.Count.Should().Be(this.documentsBeforeSave.Count - minusDocument);
        }

        [Given("the size of the document is null")]
        public void GivenTheSizeOfTheDocumentIsNull()
        {
            this.documentDto.size = null;
        }

        [Given(@"the document we try to delete has id (.*)")]
        public void GivenTheDocumentWeTryToDeleteHasId(int documentId)
        {
            this.documentToDeleteId = documentId;
        }

        [When(@"the document is deleted")]
        public void WhenTheDocumentIsDeleted()
        {
            this.actionResult = this.documentController.deleteDocument(this.documentToDeleteId).Result;
        }

        [When("we try to download a document")]
        public void WhenWeTryToDownloadADocument()
        {
            Document document = this.documentsBeforeSave.First();
            this.actionResult = this.documentController.downloadDocument(document.Id).Result;
        }

        [When(@"we download the document with that id")]
        public void WhenWeDownloadTheDocumentWithThatId()
        {
            this.actionResult = this.documentController.downloadDocument(this.documentToDownload).Result;
        }


        [Then(@"the response should be (.*)")]
        public void ThenTheResponseShouldBe(int responseStatus)
        {
            if (responseStatus == 200)
            {
                this.actionResult.Result.Should().BeOfType<OkObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 400)
            {
                this.actionResult.Result.Should().BeOfType<BadRequestObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
        }

        [Given(@"we try to dowload a document with id (.*)")]
        public void GivenWeTryToDowloadADocumentWithId(int documentId)
        {
            this.documentToDownload = documentId;
        }
    }
}
