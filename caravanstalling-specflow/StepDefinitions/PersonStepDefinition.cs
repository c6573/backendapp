﻿using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.DtoModels;
using Caravanstalling_WebAPI.Repositories;
using Caravanstalling_WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace caravanstallingSpecflow.StepDefinitions
{
    [Binding]
    internal class PersonStepDefinition
    {
        private CustomerDto customerDto;
        private readonly IPersonController personController;
        private ActionResult<CustomerDto> actionResult;
        private readonly IPersonService personService;
        private int customerId;
        private readonly Random random;

        public PersonStepDefinition(IPersonController personController,
            IPersonService personService) 
        {
            this.personController = personController;
            this.personService = personService;
            customerDto = new CustomerDto();
            this.random = new Random();
        }

        [Given(@"We want to add a new customer with firstname (.*)")]
        public void GivenWeWantToAddANewCustomerWithFirstname(string firstName)
        {
            this.setfirstname(firstName);
        }

        [Given(@"the customers last name is (.*)")]
        public void GivenTheCustomersLastNameIs(string lastName)
        {
            this.setLastName(lastName);
        }

        [Given(@"the customers lives on (.*) : (.*), (.*), (.*)")]
        public void GivenTheCustomersLivesOnTestStraatABUtrecht(string streetName, string houseNumber, string postalCode, string city)
        {
            this.customerDto.Street = streetName;
            this.customerDto.HouseNumber = houseNumber;
            this.customerDto.PostalCode = postalCode;
            this.customerDto.City = city;
        }


        [Given(@"the customers mobile phone number is (.*)")]
        public void GivenTheCustomersMobilePhoneNumberIs(string phoneNumber)
        {
            this.setPhonenumber(phoneNumber);
        }

        [Given(@"the customers unique email is (.*)")]
        public void GivenTheCustomersUniqueEmailIs(string emailAddress)
        {
            if (emailAddress != "empty")
            {
                int randomValue = this.random.Next(100);
                this.customerDto.Email = randomValue + emailAddress;
            }
            else
            {
                this.customerDto.Email = null;
            }
        }


        [Given(@"the customers email is (.*)")]
        public void GivenTheCustomersEmailIs(string emailAddress)
        {
            this.setEmail(emailAddress);
        }

        [When(@"we save this customer")]
        public void WhenWeSaveThisCustomer()
        {
            this.actionResult = this.personController.AddPerson(this.customerDto);
        }

        [Then(@"the customer response code should be (.*)")]
        public void ThenTheCustomerResponseCodeShouldBe(int responseStatus)
        {
            if (responseStatus == 200)
            {
                this.actionResult.Result.Should().BeOfType<OkObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 400)
            {
                this.actionResult.Result.Should().BeOfType<BadRequestObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
        }

        [Given(@"we want to update the customer with id (.*)")]
        public void GivenWeWantToUpdateTheCustomerWithId(int customerId)
        {
            this.customerDto = this.personService.findPersonById(customerId);
        }

        [When(@"we set his firstname to (.*) and save")]
        public void WhenWeSetHisFirstnameToAndSave(string firstName)
        {
            this.customerDto.FirstName = firstName;
            this.actionResult = this.personController.UpdatePerson(this.customerDto);
        }

        [Given(@"we want to find customer with id (.*)")]
        public void GivenWeWantToFindCustomerWithId(int customerId)
        {
            this.customerId = customerId;
        }

        [When(@"we retrieve the customer")]
        public void WhenWeRetrieveTheCustomer()
        {
            this.actionResult = this.personController.Get(this.customerId);
        }

        [Then(@"it is expected that his firstname is (.*)")]
        public void ThenItIsExpectedThatHisFirstnameIsKees(string firstName)
        {
            this.actionResult?.Value?.FirstName.Should().Be(firstName);
        }

        [When(@"we set the id of the customer to (.*)")]
        public void WhenWeSetTheIdOfTheCustomerTo(int customerId)
        {
            this.customerDto.Id = customerId;
            this.actionResult = this.personController.UpdatePerson(this.customerDto);
        }

        [When(@"we set the firstname of the customer to (.*)")]
        public void WhenWeSetTheFirstnameOfTheCustomerTo(string firstname)
        {
            this.setfirstname(firstname);
            this.actionResult = this.personController.UpdatePerson(this.customerDto);
        }

        [When(@"we set the lastname of the customer to (.*)")]
        public void WhenWeSetTheLastnameOfTheCustomerTo(string lastname)
        {
            this.setLastName(lastname);
            this.actionResult = this.personController.UpdatePerson(this.customerDto);
        }

        [When(@"we set the email of the customer to (.*)")]
        public void WhenWeSetTheEmailOfTheCustomerTo(string email)
        {
            this.setEmail(email);
            this.actionResult = this.personController.UpdatePerson(this.customerDto);
        }

        [When(@"we set the phonenumber of the customer to (.*)")]
        public void WhenWeSetThePhonenumberOfTheCustomerTo(string phoneNumber)
        {
            this.setPhonenumber(phoneNumber);
            this.actionResult = this.personController.UpdatePerson(this.customerDto);
        }

        private void setfirstname(string firstName) 
        {
            if (firstName != "empty")
            {
                this.customerDto.FirstName = firstName;
            }
            else
            {
                this.customerDto.FirstName = null;
            }
        }

        private void setLastName(string lastName)
        {
            if (lastName != "empty")
            {
                this.customerDto.LastName = lastName;
            }
            else
            {
                this.customerDto.LastName = null;
            }
        }

        private void setPhonenumber(string phoneNumber)
        {
            if (phoneNumber != "empty")
            {
                this.customerDto.PhoneNumber = phoneNumber;
            }
            else
            {
                this.customerDto.PhoneNumber = null;
            }
        }

        private void setEmail(string emailAddress) 
        {
            if (emailAddress != "empty")
            {
               
                this.customerDto.Email = emailAddress;
            }
            else
            {
                this.customerDto.Email = null;
            }
        }
    }
}
