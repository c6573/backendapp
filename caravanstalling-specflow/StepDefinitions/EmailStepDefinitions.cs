﻿using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.DtoModels;
using Caravanstalling_WebAPI.Models;
using Caravanstalling_WebAPI.Repositories;
using Caravanstalling_WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace caravanstallingSpecflow.StepDefinitions
{
    [Binding]
    internal class EmailStepDefinitions
    {
        private readonly IEmailController emailController;
        private readonly IEmailRepository emailRepository;
        private int customerId;
        private string emailType;
        private List<ScheduledEmail> scheduledEmails;
        private List<ScheduledEmail> newScheduledEmails;
        private int scheduledEmailsAmount;
        private ActionResult actionResult;
        private ActionResult<List<EmailTemplateDto>> templateActionResult;
        private EmailTemplateDto emailTemplate;
        private readonly IEmailTemplateRepository emailTemplateRepository;

        public EmailStepDefinitions(IEmailController emailController,
            IEmailRepository emailRepository,
            IEmailTemplateRepository emailTemplateRepository) 
        {
            this.emailController = emailController;
            this.emailRepository = emailRepository;
            this.scheduledEmails = new List<ScheduledEmail>();
            this.newScheduledEmails = new List<ScheduledEmail>();
            this.emailTemplate = new EmailTemplateDto();
            this.emailTemplateRepository = emailTemplateRepository;
        }

        [Given("the customer id for the scheduled email is (.*)")]
        public void GivenTheCustomerIdForTheScheduledEmailIs(int customerId)
        {
            this.customerId = customerId;
        }

        [Given("we want to schedule a email with type (.*)")]
        public void GivenWeWantToScheduleAEmailWithTypeInvoice(string emailType)
        {
            this.emailType = emailType;
        }

        [When("the email is scheduled")]
        public void WhenTheEmailIsScheduled()
        {
            this.scheduledEmails = this.emailRepository.GetScheduledEmails().Result;
            if (this.scheduledEmails == null)
            {
                this.scheduledEmailsAmount = 0;
            }
            else
            {
                this.scheduledEmailsAmount = this.scheduledEmails.Count();
            }

            this.actionResult = this.emailController.sendEmail(this.emailType, this.customerId).Result;
        }

        [Then(@"the list of scheduled emails should be \+ (.*)")]
        public void ThenTheListOfScheduledEmailsShouldBe(int extraScheduledEmail)
        {
            this.newScheduledEmails = this.emailRepository.GetScheduledEmails().Result;

            this.newScheduledEmails.Count.Should().Be(this.scheduledEmailsAmount + extraScheduledEmail);
        }

        [Then(@"the added scheduled email should have status (.*)")]
        public void ThenTheAddedScheduledEmailShouldHaveStatusPending(string status)
        {
            var scheduledEmail = this.newScheduledEmails.Last();
            scheduledEmail.state.Should().Be(status);
        }

        [Then(@"the amount reminded should be (.*)")]
        public void ThenTheAmountRemindedShouldBe(int numReminded)
        {
            var scheduledEmail = this.newScheduledEmails.Last();
            scheduledEmail.numReminded.Should().Be(numReminded);
        }



        [Then(@"the email response should be (.*)")]
        public void ThenTheResponseShouldBe(int responseStatus)
        {
            if (responseStatus == 200)
            {
                this.actionResult.Should().BeOfType<OkObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 400)
            {
                this.actionResult.Should().BeOfType<BadRequestObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
        }

        [Given(@"the template type is (.*)")]
        public void GivenTheTemplateTypeIs(string emailType)
        {
            this.emailTemplate.Id = 1;
            this.emailTemplate.emailType = emailType;
            this.emailTemplate.subject = "invoice subject";
        }

        [Given(@"the new html string is (.*)")]
        public void GivenTheNewHtmlStringIs(string htmlString)
        {
            this.emailTemplate.htmlString = htmlString;
        }

        [Given(@"the new json is (.*)")]
        public void GivenTheNewJsonIs(string jsonString)
        {
            this.emailTemplate.jsonTemplate = jsonString;
        }

        [When(@"the template is saved")]
        public void WhenTheTemplateIsSaved()
        {
            this.templateActionResult = this.emailController.saveEmailTemplate(emailTemplate).Result;
        }

        [Then(@"the emailTemplate response should be (.*)")]
        public void ThenTheEmailTemplateResponseShouldBe(int responseStatus)
        {
            if (responseStatus == 200)
            {
                this.templateActionResult.Result.Should().BeOfType<OkObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 400)
            {
                this.templateActionResult.Result.Should().BeOfType<BadRequestObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
        }
    }
}
