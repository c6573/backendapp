﻿using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.DtoModels;
using Caravanstalling_WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace caravanstallingSpecflow.StepDefinitions
{
    [Binding]
    internal class ItemStepDefinitions
    {

        private ItemDto itemDto;
        private CustomerDto customerDto;
        private readonly IItemController itemController;
        private readonly IPersonService personService;
        private readonly IItemService itemService;
        private ActionResult<ItemDto> itemActionResult;
        private ActionResult actionResult;
        private int itemId;
        private List<ItemDto> items;

        public ItemStepDefinitions(IItemController itemController,
            IPersonService personService,
            IItemService itemService) 
        {
            this.itemController = itemController;
            this.personService = personService;
            this.itemService = itemService;
            this.customerDto = new CustomerDto();
            this.itemDto = new ItemDto();
            this.items = new List<ItemDto>();
        }

        [Given(@"we want to add an item for customer with id (.*)")]
        public void GivenWeWantToAddAnItemForCustomerWithId(int customerId)
        {
            this.customerDto = this.personService.findPersonById(customerId);
            this.itemDto.Owner = customerDto;
        }

        [Given(@"the item type is (.*)")]
        public void GivenTheItemTypeIs(string type)
        {
            this.setTypeForItem(type);
        }

        [Given(@"the item lenght is (.*)")]
        public void GivenTheItemLenghtIs(int lenght)
        {
            this.itemDto.Length = lenght;
        }

        [Given(@"the licenceplate number is (.*)")]
        public void GivenTheLicenceplateNumberIs(string licenseNumber)
        {
            this.itemDto.LicensePlateNumber = licenseNumber;
        }

        [When(@"we add the item")]
        public void WhenWeAddTheItem()
        {
            this.itemActionResult = this.itemController.AddObject(this.itemDto);
        }

        [Then(@"the item response should be (.*)")]
        public void ThenTheItemResponseShouldBe(int responseStatus)
        {
            if (responseStatus == 200)
            {
                this.itemActionResult.Result.Should().BeOfType<OkObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 400)
            {
                this.itemActionResult.Result.Should().BeOfType<BadRequestObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            } else if (responseStatus == 404)
            {
                this.itemActionResult.Result.Should().BeOfType<NotFoundObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
        }

        [Given(@"we want to add an item for with no owner")]
        public void GivenWeWantToAddAnItemForWithNoOwner()
        {
            this.customerDto = null;
        }

        [Given(@"we want to update item with id (.*)")]
        public void GivenWeWantToUpdateItemWithId(int itemId)
        {
            this.itemDto = this.itemService.findItemById(itemId);
        }

        [Given(@"we set the type to (.*)")]
        public void GivenWeSetTheTypeToCamper(string type)
        {
            this.setTypeForItem(type);
        }

        [Given(@"we set the length to (.*)")]
        public void GivenWeSetTheLengthTo(int length)
        {
            this.itemDto.Length = length;
        }

        [When(@"we update the item")]
        public void WhenWeUpdateTheItem()
        {
            this.itemActionResult = this.itemController.UpdateObject(this.itemDto);
        }

        [Given(@"we want to retrieve the item with id (.*)")]
        public void GivenWeWantToRetrieveTheItemWithId(int itemId)
        {
            this.itemId = itemId;
        }

        [When(@"we retrieve the item")]
        public void WhenWeRetrieveTheItem()
        {
            this.itemActionResult = this.itemController.Get(this.itemId);
        }

        [Given(@"we want to delete a item")]
        public void GivenWeWantToDeleteAItem()
        {
            this.itemId = this.itemService.getAllItems()[0].Id;
        }

        [When(@"we delete this item")]
        public void WhenWeDeleteThisItem()
        {
            this.actionResult = this.itemController.DeleteItem(this.itemId).Result;
        }

        [Then(@"the action response should be (.*)")]
        public void ThenTheActionResponseShouldBe(int responseStatus)
        {
            if (responseStatus == 200)
            {
                this.actionResult.Should().BeOfType<OkResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 400)
            {
                this.actionResult.Should().BeOfType<BadRequestObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
            else if (responseStatus == 404)
            {
                this.actionResult.Should().BeOfType<NotFoundObjectResult>()
                    .Which.StatusCode.Should().Be(responseStatus);
            }
        }

        [Given(@"we want to delete a item with id (.*)")]
        public void GivenWeWantToDeleteAItemWithId(int itemId)
        {
            this.itemId = itemId;
        }


        private void setTypeForItem(string type) 
        {
            if (type != "empty")
            {
                this.itemDto.Type = type;
            }
            else
            {
                this.itemDto.Type = null;
            }
        }
    }
}
