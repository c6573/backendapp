﻿using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.Data;
using Caravanstalling_WebAPI.Helpers;
using Caravanstalling_WebAPI.Mappers;
using Caravanstalling_WebAPI.Repositories;
using Caravanstalling_WebAPI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SolidToken.SpecFlow.DependencyInjection;
using System.Data.SqlClient;

namespace caravanstallingSpecflow.StepDefinitions
{
    public static class TestDependancies
    {
        [ScenarioDependencies]
        public static IServiceCollection CreateServices()
        {
            var services = new ServiceCollection();

            services.AddScoped<IEmailHelper, EmailHelper>();
            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IPersonController, PersonController>();
            services.AddScoped<ICustomerMapper, CustomerMapper>();
            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IItemMapper, ItemMapper>();
            services.AddScoped<IItemController, ItemController>();
            services.AddScoped<IEmailRepository, EmailRepository>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IEmailTemplateRepository, EmailTemplateRepository>();
            services.AddScoped<IEmailTemplateMapper, EmailTemplateMapper>();
            services.AddScoped<IEmailController, EmailController>();
            services.AddScoped<ISettingRepository, SettingRepository>();
            services.AddScoped<ISettingService, SettingService>();
            services.AddScoped<ISettingsMapper, SettingsMapper>();
            services.AddScoped<IDocumentRepository, DocumentRepository>();
            services.AddScoped<IDocumentService, DocumentService>();
            services.AddScoped<IDocumentMapper, DocumentMapper>();
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<INoteService, NoteServices>();
            services.AddScoped<INoteMapper, NoteMapper>();
            services.AddScoped<INoteController, NotesController>();
            services.AddScoped<IDocumentController, DocumentController>();

            var connectionString = new SqlConnectionStringBuilder()
            {
                DataSource = "localhost",
                InitialCatalog = "caravanstalling",
                UserID = "root",
                Password = "root"
            }.ConnectionString;
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            });

            return services;
        }
    }
}
