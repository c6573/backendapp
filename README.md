# Backend for Caravanstalling-Elst

This webapi project is based on .NET 6.  
So please make sure u have this SDK installed first. We suggest developing using Visual Studio 2022 which will include this SDK by default.

Open the solution and run the Docker-compose option for a full docker enabled development environment.  
This includes the webapi container and a mysql container for the database.  
Migrations are executed automatically, however when starting for the first time it is likely that the mysql container is not yet running before the webapi tries to connect to the database. This is a known issue which we haven't taken the time for to solve. Simply restart the docker compose and the second time it will work.

OpenAPI (Swagger) documentation of the provided functionality is included and available under the relative url `/swagger` which will automatically load locally.

### Configuration
The only configuration we have at this time is the url to the MySQL database.
U can find this setting in the `appsettings.json` file inside the webapi project.  
The default value points to a MySQL container in either the docker-compose file or the kubernetes service which is specified in the deploy/deployment.yaml file.  
In production you can override this setting by providing an environment variable called `ConnectionStrings__caravanstalling` containing the connectionstring towards your production database.

### Integration test
The intergration tests run on the SpecFlow framework https://specflow.org/. In order to succesfully run the test you should have a connection to the database.
If connected you should run the BaseTestData.sql script, this script can be found in the resources folder of the Caravanstalling-WebAPI project. To run the test
go to Test -> Test explorer.