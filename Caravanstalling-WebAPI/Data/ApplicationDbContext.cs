﻿namespace Caravanstalling_WebAPI.Data
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) :
            base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.Entity<EmailTemplate>()
                .HasData(
                    new EmailTemplate { Id = 1, subject = "invoice subject", emailType = "invoice", htmlString = "", jsonTemplate ="" },
                    new EmailTemplate { Id = 2, subject = "terms subject", emailType = "terms", htmlString = "", jsonTemplate ="" }
                );

            // TODO dit moet straks weg. Beter om een import sql te hebben.
            modelBuilder.Entity<Settings>()
                .HasData(
                    new Settings { id = 1 , type = "email", label =  "sender", value = "caravanstalling elst"},
                    new Settings { id = 2 , type = "email", label =  "sender-email", value = "visualnationnl@gmail.com"},
                    new Settings { id = 3 , type = "email", label =  "smtp-client", value = "smtp.gmail.com"},
                    new Settings { id = 4 , type = "email", label =  "login-name", value = "visualnationnl" },
                    new Settings { id = 5 , type = "email", label =  "login-password", value = "yobtmojlyyxmcqno" },
                    new Settings { id = 6 , type = "email", label =  "smtp-port", value = "465" }
                );
        }

        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ScheduledEmail> ScheduledEmail { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<Document> Document { get; set; }
        public virtual DbSet<Notes> Notes { get; set; }
    }
}
