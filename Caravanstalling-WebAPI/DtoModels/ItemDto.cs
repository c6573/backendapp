﻿namespace Caravanstalling_WebAPI.DtoModels
{
    public class ItemDto
    {
        public int Id { get; set; }
        public string? LicensePlateNumber { get; set; }
        public float Length { get; set; }
        public bool BikeCarrier { get; set; }
        public string Type { get; set; }
        public virtual List<CustomerDto>? SecondaryOwners { get; set; }
        public virtual CustomerDto? Owner { get; set; }
        public virtual List<DocumentDto>? Documents { get; set; }
        public virtual NoteDto? notes { get; set; }

        public ItemDto()
        {
            this.SecondaryOwners = new List<CustomerDto>();
        }
    }
}
