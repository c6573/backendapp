﻿namespace Caravanstalling_WebAPI.DtoModels
{
    public class CustomerDto
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Street { get; set; }
        public string? HouseNumber { get; set; }
        public string? PostalCode { get; set; }
        public string? City { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Email { get; set; }
        public virtual List<ItemDto>? Items { get; set; }
        public virtual List<DocumentDto>? Documents { get; set; }
        public virtual NoteDto? Notes { get; set; }
    }
}
