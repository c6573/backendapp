﻿namespace Caravanstalling_WebAPI.DtoModels
{
    public class NoteDto
    {
        public int id { get; set; }
        public string note { get; set; }
        public int? customerId { get; set; }
        public int? itemId { get; set; }
    }
}
