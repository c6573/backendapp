﻿namespace Caravanstalling_WebAPI.DtoModels
{
    public class SettingsDto
    {
        public int id { get; set; }
        public string type { get; set; }
        public string label { get; set; }
        public string value { get; set; }
    }
}
