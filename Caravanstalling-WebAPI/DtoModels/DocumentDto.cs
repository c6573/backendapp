﻿namespace Caravanstalling_WebAPI.DtoModels
{
    public class DocumentDto
    {
        public int id { get; set; }
        public int? customersId { get; set; }
        public int? itemId { get; set; }
        public string uploadDate { get; set; }
        public string size { get; set; }
        public string fileName { get; set; }
        public string? fileString { get; set; }
    }
}
