﻿namespace Caravanstalling_WebAPI.DtoModels
{
    public class EmailTemplateDto
    {
        public int? Id { get; set; }
        public string subject { get; set; }
        public string emailType { get; set; }
        public string? htmlString { get; set; }
        public string? jsonTemplate { get; set; }
    }
}
