global using System.ComponentModel.DataAnnotations;
global using System.ComponentModel.DataAnnotations.Schema;
global using Microsoft.EntityFrameworkCore;
global using Caravanstalling_WebAPI.Models;
global using Caravanstalling_WebAPI.DtoModels;
global using Caravanstalling_WebAPI.Data;
global using Caravanstalling_WebAPI.Services;
global using Caravanstalling_WebAPI.Repositories;
global using Caravanstalling_WebAPI.Mappers;
global using Caravanstalling_WebAPI.Constants;
global using AutoMapper;
global using MailKit.Net.Smtp;
global using MailKit;
global using MimeKit;
using Quartz.Spi;
using Caravanstalling_WebAPI.Cron.JobFactory;
using Quartz;
using Quartz.Impl;
using Caravanstalling_WebAPI.Cron.Jobs;
using Caravanstalling_WebAPI.Cron.Models;
using Caravanstalling_WebAPI.Cron.Scheduler;
using Caravanstalling_WebAPI.Controllers;
using Caravanstalling_WebAPI.Helpers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
var connectionString = builder.Configuration.GetConnectionString("caravanstalling");
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IEmailHelper, EmailHelper>();
builder.Services.AddScoped<IPersonService, PersonService>();
builder.Services.AddScoped<IPersonRepository, PersonRepository>();
builder.Services.AddScoped<IPersonController, PersonController>();
builder.Services.AddScoped<ICustomerMapper, CustomerMapper>();
builder.Services.AddScoped<IItemService, ItemService>();
builder.Services.AddScoped<IItemRepository, ItemRepository>();
builder.Services.AddScoped<IItemMapper, ItemMapper>();
builder.Services.AddScoped<IItemController, ItemController>();
builder.Services.AddScoped<IEmailRepository, EmailRepository>();
builder.Services.AddScoped<IEmailService, EmailService>();
builder.Services.AddScoped<IEmailTemplateRepository, EmailTemplateRepository>();
builder.Services.AddScoped<IEmailTemplateMapper, EmailTemplateMapper>();
builder.Services.AddScoped<IEmailController, EmailController>();
builder.Services.AddScoped<ISettingRepository, SettingRepository>();
builder.Services.AddScoped<ISettingService, SettingService>();
builder.Services.AddScoped<ISettingsMapper, SettingsMapper>();
builder.Services.AddScoped<IDocumentRepository, DocumentRepository>();
builder.Services.AddScoped<IDocumentService, DocumentService>();
builder.Services.AddScoped<IDocumentMapper, DocumentMapper>();
builder.Services.AddScoped<INoteRepository, NoteRepository>();
builder.Services.AddScoped<INoteService, NoteServices>();
builder.Services.AddScoped<INoteMapper, NoteMapper>();
builder.Services.AddScoped<INoteController, NotesController>();
builder.Services.AddScoped<IDocumentController, DocumentController>();



builder.Services.AddSingleton<IJobFactory, JobFactory>();
builder.Services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
builder.Services.AddSingleton<QuartzJobRunner>();

builder.Services.AddScoped<SendEmailJob>();

List<JobMetadata> jobMetadataList = new List<JobMetadata>();
jobMetadataList.Add(new JobMetadata(Guid.NewGuid(), typeof(SendEmailJob), "send scheduled emails", "0 0/15 * 1/1 * ? *"));

builder.Services.AddSingleton(jobMetadataList);

builder.Services.AddHostedService<EmailScheduler>();

//services cors
builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

using (var scope = app.Services.CreateScope())
{
    var dataContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    dataContext.Database.Migrate();
}

//app.UseHttpsRedirection();

app.UseCors("corsapp");

app.UseAuthorization();

app.MapControllers();

app.Run();
