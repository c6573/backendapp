﻿using System.Data.Entity.Core;

namespace Caravanstalling_WebAPI.Services
{
    public class ItemService : IItemService
    {
        public readonly IItemRepository itemRepository;
        public readonly IItemMapper objectMapper;
        public readonly ICustomerMapper customerMapper;
        public readonly IPersonRepository personRepository;
        private string[] itemTypes = {
            "Auto",
            "Boot",
            "Camper",
            "Caravan",
            "Vouwwagen"
        };

        public ItemService(IItemRepository itemRepository, IItemMapper objectMapper, ICustomerMapper customerMapper, IPersonRepository personRepository) 
        {
            this.itemRepository = itemRepository;
            this.objectMapper = objectMapper;
            this.customerMapper = customerMapper;
            this.personRepository = personRepository;
        }

        public ItemDto CreateItem(ItemDto itemDto)
        {
            if (!String.IsNullOrEmpty(this.validateItem(itemDto))) 
            {
                throw new ValidationException(this.validateItem(itemDto));
            }

            Item newItem = objectMapper.toItem(itemDto);
            newItem.Customer = personRepository.getPersonById(itemDto.Owner.Id);

            Item savedItem = itemRepository.AddItem(newItem).Result;

            return objectMapper.toDto(savedItem);
        }

        public async Task DeleteItem(int id)
        {
            await itemRepository.DeleteItem(id);
        }

        public ItemDto findItemById(int Id)
        {
            Item item = itemRepository.GetItemById(Id).Result;
            if (item == null)
            {
                throw new ObjectNotFoundException("Het object dat je probeert te vinden bestaat niet");
            }
            
            return item == null ? null : objectMapper.toDto(item);
        }

        public List<ItemDto> getAllItems()
        {
            List<Item> items = itemRepository.getAllItems().Result;
            List<ItemDto> itemDtos = new();

            foreach (Item item in items)
            {
                itemDtos.Add(objectMapper.toDto(item));
            }

            return itemDtos;

        }

        public ItemDto UpdateItem(ItemDto itemDto)
        {
            if (!String.IsNullOrEmpty(this.validateItem(itemDto)))
            {
                throw new ValidationException(this.validateItem(itemDto));
            }

            Item itemToUpdate = objectMapper.toItem(itemDto);
            Item updatedItem = itemRepository.UpdateItem(itemToUpdate).Result;

            return objectMapper.toDto(updatedItem);
        }

        private string validateItem(ItemDto itemDto) 
        {
            if (itemDto.Owner == null)
            {
                return "Er is geen eigenaar gekoppeld aan het object";
            }
            else if (String.IsNullOrEmpty(itemDto.Type))
            {
                return "Er is geen type gekozen voor het object";
            }
            else if (itemDto.Length == 0) 
            {
                return "Er is geen lengte gekozen voor het object";
            }
            else if (!this.itemTypes.Contains(itemDto.Type))
            {
                return "Het type object wat je probeert toe te voegen bestaat niet";
            }
            return null;
        }
    }
}
