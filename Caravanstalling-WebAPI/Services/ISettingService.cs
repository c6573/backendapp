﻿namespace Caravanstalling_WebAPI.Services
{
    public interface ISettingService
    {
        public Task<List<Settings>> getSettingsByType(string type);
    }
}
