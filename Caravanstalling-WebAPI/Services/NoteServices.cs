﻿namespace Caravanstalling_WebAPI.Services
{
    public class NoteServices: INoteService
    {
        private readonly INoteRepository noteRepository;
        private readonly INoteMapper noteMapper;

        public NoteServices(INoteRepository noteRepository, INoteMapper noteMapper) 
        {
            this.noteRepository = noteRepository;
            this.noteMapper = noteMapper;
        }

        public async Task<NoteDto> saveNotes(NoteDto noteDto)
        {
            Notes note = new Notes();

            if (String.IsNullOrEmpty(noteDto.note)) 
            {
                await this.noteRepository.deleteNote(noteDto.id);
                return null;
            }

            if (noteDto.customerId != null)
            {
                if (noteDto.id == 0)
                {
                    note = this.noteRepository.getNoteForCustomer(noteDto.customerId).Result;

                    if (note != null)
                    {
                        note.note = noteDto.note;
                    }
                    else
                    {
                        note = this.noteMapper.toNote().Map<Notes>(noteDto);
                    }
                }
                else
                {
                    note = this.noteMapper.toNote().Map<Notes>(noteDto);
                }

                return this.noteMapper.toDto().Map<NoteDto>(this.noteRepository.saveNoteForCustomer(note, noteDto.customerId).Result);
            }
            else 
            {
                if (noteDto.id == 0)
                {
                    note = this.noteRepository.getNotesForItem(noteDto.itemId).Result;
                    if (note != null)
                    {
                        note.note = noteDto.note;
                    }
                    else
                    {
                        note = this.noteMapper.toNote().Map<Notes>(noteDto);
                    }
                }
                else
                {
                    note = this.noteMapper.toNote().Map<Notes>(noteDto);
                }

                return this.noteMapper.toDto().Map<NoteDto>(this.noteRepository.saveNoteforItem(note, noteDto.itemId).Result);
            }
        }
    }
}
