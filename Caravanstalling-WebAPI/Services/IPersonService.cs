﻿namespace Caravanstalling_WebAPI.Services
{
    public interface IPersonService
    {
        public List<CustomerDto> getAllPersons();
        public CustomerDto findPersonById(int Id);
        public CustomerDto findPersonByIdWithoutItems(int Id);
        public CustomerDto CreatePerson(CustomerDto person); 
        public CustomerDto UpdatePerson(CustomerDto person);
        //public List<DtoPerson> DeletePerson(int id); 
        public Customer getCustomerByEmailAdress(string emailAdress);
    }
}
