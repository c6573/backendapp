﻿namespace Caravanstalling_WebAPI.Services
{
    public interface IItemService
    {
        public List<ItemDto> getAllItems();
        public ItemDto findItemById(int Id);
        public ItemDto CreateItem(ItemDto itemDto);
        public ItemDto UpdateItem(ItemDto itemDto);
        public Task DeleteItem(int id); 
    }
}
