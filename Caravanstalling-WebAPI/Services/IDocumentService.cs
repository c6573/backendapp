﻿namespace Caravanstalling_WebAPI.Services
{
    public interface IDocumentService
    {
        public Task uploadDocument(DocumentDto document);
        public Task deleteDocument(int id);
        public Task<DocumentDto> downloadDocument(int id);
    }
}