﻿namespace Caravanstalling_WebAPI.Services
{
    public class SettingService: ISettingService
    {
        public readonly ISettingRepository settingRepository;
        public SettingService(ISettingRepository settingRepository) 
        {
            this.settingRepository = settingRepository;
        }

        public async Task<List<Settings>> getSettingsByType(string type) 
        {
            return await this.settingRepository.getSettingsByType(type);
        }
    }
}
