﻿namespace Caravanstalling_WebAPI.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IDocumentRepository documentRepository;
        private readonly IDocumentMapper documentMapper;

        public DocumentService(IDocumentRepository documentRepository, IDocumentMapper documentMapper)
        {
            this.documentRepository = documentRepository;
            this.documentMapper = documentMapper;
        }

        public async Task uploadDocument(DocumentDto documentDto)
        {

            if (String.IsNullOrEmpty(Path.GetExtension(documentDto.fileName))) 
            {
                throw new ValidationException("het gekozen bestand heeft geen bestands extensie");
            }
            
            if (documentDto.size == "0" || documentDto.size == null)
            {
                throw new ValidationException("Het gekozen bestand is leeg");
            }

            Document documentToUpload = this.documentMapper.ToDocument(documentDto);
            documentToUpload.uploadDate = DateTime.Now.ToString("MM/dd/yyyy");
            
            if (documentDto.customersId != null)
            {
                await this.documentRepository.saveDocumentForCustomer(documentToUpload, documentDto.customersId);
            }
            else 
            {
                await this.documentRepository.saveDocumentForItem(documentToUpload, documentDto.itemId);
            }
        }

        public async Task deleteDocument(int id) 
        {
            Document document = this.documentRepository.getDocumentById(id).Result;

            if (document == null)
            {
                throw new ValidationException("Het document dat je probeert te verwijderen bestaat niet");
            }
            else
            {
                await this.documentRepository.deleteDocument(id);
            }
        }

        public async Task<DocumentDto> downloadDocument(int id) 
        {
            Document document = await this.documentRepository.getDocumentById(id);

            if (document == null) 
            {
                throw new ValidationException("Het document wat je probeert te verwijderen bestaat niet");
            }

            return this.documentMapper.ToDtoWithfileString(document);
        }
    }
}
