﻿using Caravanstalling_WebAPI.Helpers;
using Quartz;
using System.Data.Entity.Core;
using System.Text.RegularExpressions;

namespace Caravanstalling_WebAPI.Services
{
    public class PersonService : IPersonService
    {
        public readonly IPersonRepository PersonRepository;
        public readonly ICustomerMapper customerMapper;
        private readonly IEmailHelper emailHelper;
        private Regex phoneNumberRegex = new Regex(@"^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?\d)((\s|\s?-\s?)?\d)((\s|\s?-\s?)?\d)\s?\d\s?\d\s?\d\s?\d\s?\d$");

        public PersonService(IPersonRepository personRepository, ICustomerMapper customerMapper, IEmailHelper emailHelper)
        {
            this.PersonRepository = personRepository;
            this.customerMapper = customerMapper;
            this.emailHelper = emailHelper;
        }

        public List<CustomerDto> getAllPersons()
        {
            List<Customer> customers = PersonRepository.getAllPersons().Result;
            List<CustomerDto> customerDtos = new List<CustomerDto>();

            foreach (Customer customer in customers)
            {
                customerDtos.Add(this.customerMapper.ToDtoWithObjects(customer));
            }

            return customerDtos;
        }

        public CustomerDto findPersonById(int Id)
        {
            Customer customer = PersonRepository.getPersonById(Id);
            if (customer == null)
            {
                throw new ObjectNotFoundException("De klant die je probeert te vinden bestaat niet");
            }

            return this.customerMapper.ToDtoWithObjects(customer);
        }

        public CustomerDto findPersonByIdWithoutItems(int Id)
        {
            Customer customer = PersonRepository.getPersonById(Id);
            return this.customerMapper.ToDto().Map<CustomerDto>(customer);
        }

        public CustomerDto CreatePerson(CustomerDto customerDto)
        {

            if (this.checkIfCustomerAlreadyExists(customerDto.Email))
            {
                throw new ObjectAlreadyExistsException("Deze klant bestaat al in het systeem");
            }

            if (!String.IsNullOrEmpty(this.customerValidation(customerDto)))
            {
                throw new ValidationException(this.customerValidation(customerDto));
            }

            Customer newCustomer = this.customerMapper.toCustomer().Map<Customer>(customerDto);
            Customer customer = PersonRepository.AddCustomer(newCustomer).Result;

            return this.customerMapper.ToDtoWithObjects(customer);
        }

        public CustomerDto UpdatePerson(CustomerDto customerDto)
        {
            if (customerDto.Id == 0)
            {
                throw new ObjectNotFoundException("De klant die je probeert te updaten bestaat niet");
            }

            if (!String.IsNullOrEmpty(this.customerValidation(customerDto)))
            {
                throw new ValidationException(this.customerValidation(customerDto));
            }

            Customer customerToUpdate = this.customerMapper.toCustomer().Map<Customer>(customerDto);
            Customer customer = PersonRepository.UpdateCustomer(customerToUpdate).Result;

            return this.customerMapper.ToDtoWithObjects(customer);
        }

        public Customer getCustomerByEmailAdress(string emailAdress)
        {
            return this.PersonRepository.getCustomerByEmailAdress(emailAdress).Result;
        }

        private bool checkIfCustomerAlreadyExists(string email)
        {
            Customer customer = this.getCustomerByEmailAdress(email);
            if (customer != null)
            {
                return true;
            }
            return false;
        }

        private string customerValidation(CustomerDto customerDto) 
        {
            if (String.IsNullOrEmpty(customerDto.FirstName)
                || String.IsNullOrEmpty(customerDto.LastName)
                || String.IsNullOrEmpty(customerDto.Email)
                || String.IsNullOrEmpty(customerDto.PhoneNumber))
            {
                return "Niet alle verplichte velden zijn ingevuld";
            }

            if (!this.emailHelper.checkIfEmailIsvalid(customerDto.Email))
            {
                return "Email Addres is niet geldig";
            }

            if (!this.phoneNumberRegex.IsMatch(customerDto.PhoneNumber))
            {
               return "Opgegeven telefoon nummer is niet geldig";
            }
            return null;
        }
    }
}
