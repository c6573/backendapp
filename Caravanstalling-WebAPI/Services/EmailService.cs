﻿using Caravanstalling_WebAPI.Helpers;
using MailKit.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Caravanstalling_WebAPI.Services
{
    public class EmailService : IEmailService
    {
        public readonly IPersonService personService;
        public readonly IEmailRepository emailRepository;
        public readonly IEmailTemplateRepository emailTemplateRepository;
        public readonly IEmailTemplateMapper emailTemplateMapper;
        public readonly ISettingService settingService;
        private readonly IEmailHelper emailHelper;
        private Regex htmlRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");

        public EmailService(IPersonService personService,
            IEmailRepository emailRepository,
            IEmailTemplateRepository emailTemplateRepository,
            IEmailTemplateMapper emailTemplateMapper,
            ISettingService settingService,
            IEmailHelper emailHelper) 
        {
            this.personService = personService;
            this.emailRepository = emailRepository;
            this.emailTemplateRepository = emailTemplateRepository;
            this.emailTemplateMapper = emailTemplateMapper;
            this.settingService = settingService;
            this.emailHelper = emailHelper;
        }

        public async Task sendEmail(string emailType, int customerId)
        {
            CustomerDto customer;

            if (customerId != 0)
            {
                customer = this.personService.findPersonById(customerId);
            }
            else
            {
                throw new ValidationException("customer id is leeg");
            }
            

            if (this.emailHelper.checkIfEmailIsvalid(customer.Email) && checkIfEmailTemplateExists(emailType))
            {
                await scheduleEmail(emailType, customer.Email);
            }
            else
            {
                throw new ValidationException("Het email adress is niet valide of het gekozen email template bestaat niet"); 
            }
        }

        private Boolean checkIfEmailTemplateExists(string emailType) 
        {
            List<EmailTemplate> emailTemplates = this.emailTemplateRepository.GetEmailTemplates().Result;
            Boolean templateExists = false;

            foreach (EmailTemplate emailTemplate in emailTemplates)
            {
                if (emailTemplate.emailType == emailType) 
                {
                    templateExists = true;
                }
            }
            return templateExists;
        }

        private async Task scheduleEmail(string emailType, string emailAdress) 
        {
            // TODO de tijd waarop emails verstuurd worden kan misschien verschillen per emailType
            ScheduledEmail email = new ScheduledEmail(emailAdress, EmailStatus.Pending, emailType, 0, DateTime.Now.AddMinutes(10));
            await this.emailRepository.scheduleEmail(email);
        }

        public async Task sendScheduledEmail()
        {
            List<ScheduledEmail> scheduledEmails = emailRepository.GetScheduledEmails().Result;
            foreach (ScheduledEmail scheduledEmail in scheduledEmails)
            {
                if (this.allowedToSendEmail(scheduledEmail)) 
                {
                    sendEmail(scheduledEmail);
                }
            }
            await this.emailRepository.updateScheduledEmail(scheduledEmails);
        }

        private void sendEmail(ScheduledEmail scheduledEmail) 
        {
            List<Settings> emailSettings = this.settingService.getSettingsByType("email").Result;

            MimeMessage message = new MimeMessage();

            message.From.Add(new MailboxAddress(emailSettings?.Find(s => s.label == "sender")?.value, emailSettings?.Find(s => s.label == "sender-email")?.value));
            message.To.Add(MailboxAddress.Parse(scheduledEmail.emailAdress));

            EmailTemplate emailTemplate = this.emailTemplateRepository.getEmailTemplateByType(scheduledEmail.emailType).Result;
            message.Subject = emailTemplate.subject;

            message.Body = new TextPart("html")
            {
                Text = createHtmlBody(scheduledEmail.emailType, scheduledEmail.emailAdress)
            };


            SmtpClient smtpClient = new SmtpClient(new ProtocolLogger("smtp.log"));

            try
            {
                smtpClient.Connect(emailSettings?.Find(s => s.label == "smtp-client")?.value, Int32.Parse(emailSettings?.Find(s => s.label == "smtp-port")?.value), SecureSocketOptions.SslOnConnect);
                smtpClient.Authenticate(emailSettings?.Find(s => s.label == "login-name")?.value, emailSettings?.Find(s => s.label == "login-password")?.value);
                smtpClient.Send(message);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            finally
            {
                smtpClient.Disconnect(true);
                smtpClient.Dispose();

                scheduledEmail.numReminded++;

                if (scheduledEmail.numReminded >= 2) 
                {
                    scheduledEmail.state = EmailStatus.Complete;
                }
            }
        }

        private Boolean allowedToSendEmail(ScheduledEmail scheduledEmail) 
        {
            System.Diagnostics.Debug.WriteLine("checking if email with emailtype: " + scheduledEmail.emailType + " is allowed to send");
            return scheduledEmail.numReminded < 2 && scheduledEmail.remindAt < DateTime.Now && scheduledEmail.state != EmailStatus.Complete;
        }

        private string createHtmlBody(string emailType, string email) 
        {
            Customer customer = this.personService.getCustomerByEmailAdress(email);
            EmailTemplate template = this.emailTemplateRepository.getEmailTemplateByType(emailType).Result;

            string finalHtmlString = replacePlaceholdersInHtml(template.htmlString, customer);

            return finalHtmlString;
        }

        private string replacePlaceholdersInHtml(string htmlString, Customer customer) 
        {
            // TODO dit moet uitgebreid worden
            htmlString.Replace("{$ClientName}", customer.FirstName + " " + customer.LastName);

            return htmlString;
        }

        public async Task<List<EmailTemplateDto>> saveTemplateData(EmailTemplateDto emailTemplateDto) 
        {
            if (!this.ValidateJson(emailTemplateDto.jsonTemplate))
            {
                throw new ValidationException("De json is niet valide");
            }

            if (!this.htmlRegex.IsMatch(emailTemplateDto.htmlString)) 
            {
                throw new ValidationException("De html is niet valide");
            }

            EmailTemplate emailTemplate = this.emailTemplateMapper.toEmailTemplate().Map<EmailTemplate>(emailTemplateDto);
            await this.emailTemplateRepository.UpdateTemplate(emailTemplate);

            return getAllEmailTemplates();
        }

        public List<EmailTemplateDto> getAllEmailTemplates()
        { 
            List<EmailTemplate> emailTemplates = this.emailTemplateRepository.GetEmailTemplates().Result;
            List<EmailTemplateDto> emailTemplateDtos = new List<EmailTemplateDto>();

            foreach (EmailTemplate emailTemplate in emailTemplates)
            {
                emailTemplateDtos.Add(this.emailTemplateMapper.toDto().Map<EmailTemplateDto>(emailTemplate));
            }

            return emailTemplateDtos;
        }

        private bool ValidateJson(string jsonString)
        {
            try
            {
                JToken.Parse(jsonString);
                return true;
            }
            catch (JsonReaderException ex)
            {
                Trace.WriteLine(ex);
                return false;
            }
        }
    }
}
