﻿namespace Caravanstalling_WebAPI.Services
{
    public interface INoteService
    {
        public Task<NoteDto> saveNotes(NoteDto noteDto);
    }
}
