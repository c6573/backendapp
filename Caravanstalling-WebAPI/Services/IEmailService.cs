﻿namespace Caravanstalling_WebAPI.Services
{
    public interface IEmailService
    {

        public Task sendEmail(string emailType, int customerId);
        public Task sendScheduledEmail();
        public Task<List<EmailTemplateDto>> saveTemplateData(EmailTemplateDto emailTemplateDto);
        public List<EmailTemplateDto> getAllEmailTemplates();
    }
}
