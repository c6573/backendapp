﻿using Microsoft.AspNetCore.Mvc;

namespace Caravanstalling_WebAPI.Controllers
{
    public interface IPersonController
    {
        public ActionResult<List<CustomerDto>> Get();
        public ActionResult<CustomerDto> Get(int id);
        public ActionResult<CustomerDto> AddPerson(CustomerDto customer);
        public ActionResult<CustomerDto> UpdatePerson(CustomerDto customer);
    }
}
