﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.Entity.Core;

namespace Caravanstalling_WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ItemController : ControllerBase, IItemController
    {
        private readonly IItemService itemService;

        public ItemController(IItemService ObjectService)
        {
            this.itemService = ObjectService;
        }

        [HttpGet]
        public ActionResult<List<ItemDto>> Get()
        {
            return Ok(this.itemService.getAllItems());
        }

        [HttpGet("{id}")]
        public ActionResult<ItemDto> Get(int id)
        {
            try
            {
                return Ok(this.itemService.findItemById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            
        }

        [HttpPost]
        public ActionResult<ItemDto> AddObject(ItemDto objectDto)
        {
            try
            {
                return Ok(this.itemService.CreateItem(objectDto));
            }
            catch (ValidationException ex)
            { 
                return BadRequest(ex.Message);
            }
            
        }

        [HttpPut]
        public ActionResult<ItemDto> UpdateObject(ItemDto objectDto)
        {
            try
            {
                return Ok(this.itemService.UpdateItem(objectDto));
            }
            catch (ValidationException ex) 
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteItem(int id) 
        {
            ItemDto item;
            try
            {
                item = itemService.findItemById(id);
            }
            catch (ObjectNotFoundException ex)
            {
                return NotFound(ex.Message);
            }

            if (item.SecondaryOwners.Count > 0)
            {
                return BadRequest("Een of meerdere secundaire eigenaren zijn nog gekoppeld aan het object, verwijder deze voordat het object verwijderd kan worden.");
            }

            await itemService.DeleteItem(id);

            return Ok(); 
            
        }
    }
}
