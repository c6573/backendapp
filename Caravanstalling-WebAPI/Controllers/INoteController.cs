﻿namespace Caravanstalling_WebAPI.Controllers
{
    public interface INoteController
    {
        public NoteDto saveNote(NoteDto noteDto);
    }
}
