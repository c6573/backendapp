﻿using Microsoft.AspNetCore.Mvc;

namespace Caravanstalling_WebAPI.Controllers
{
    public interface IEmailController
    {
        public Task<ActionResult> sendEmail(string emailType, int customerId);
        public Task<ActionResult<List<EmailTemplateDto>>> saveEmailTemplate(EmailTemplateDto emailTemplateDto);
    }
}
