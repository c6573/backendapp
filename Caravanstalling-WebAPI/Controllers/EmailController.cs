﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Caravanstalling_WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmailController : ControllerBase, IEmailController
    {
        public readonly IEmailService emailService;

        public EmailController(IEmailService emailService)
        {
            this.emailService = emailService;
        }

        [HttpPost]
        public async Task<ActionResult> sendEmail(string emailType, int customerId)
        {
            try
            {
                await this.emailService.sendEmail(emailType, customerId);
                return NoContent();
            }
            catch (ValidationException ex)
            { 
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("/save-email-template")]
        public async Task<ActionResult<List<EmailTemplateDto>>> saveEmailTemplate(EmailTemplateDto emailTemplateDto)
        {
            try
            {
                return Ok(await this.emailService.saveTemplateData(emailTemplateDto));
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        [HttpGet("/get-all-email-templates")]
        public List<EmailTemplateDto> getAllEmailTemplates()
        {
            return this.emailService.getAllEmailTemplates();
        }
    }
}
