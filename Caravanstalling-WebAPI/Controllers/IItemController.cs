﻿using Microsoft.AspNetCore.Mvc;

namespace Caravanstalling_WebAPI.Controllers
{
    public interface IItemController
    {
        public ActionResult<List<ItemDto>> Get();
        public ActionResult<ItemDto> Get(int id);
        public ActionResult<ItemDto> AddObject(ItemDto objectDto);
        public ActionResult<ItemDto> UpdateObject(ItemDto objectDto);
        public Task<ActionResult> DeleteItem(int id);
    }
}
