﻿using Microsoft.AspNetCore.Mvc;

namespace Caravanstalling_WebAPI.Controllers
{
    public interface IDocumentController
    {
        public Task<ActionResult> uploadFile(DocumentDto document);
        public Task<ActionResult> deleteDocument(int id);
        public Task<ActionResult<DocumentDto>> downloadDocument(int id);
    }
}
