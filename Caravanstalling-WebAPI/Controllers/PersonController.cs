﻿using Microsoft.AspNetCore.Mvc;
using Quartz;
using System.Data.Entity.Core;

namespace Caravanstalling_WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PersonController : ControllerBase, IPersonController
    {
        private readonly IPersonService PersonService;

        public PersonController(IPersonService personService)
        {
            this.PersonService = personService;
        }

        [HttpGet]
        public ActionResult<List<CustomerDto>> Get()
        {
            return Ok(this.PersonService.getAllPersons());
        }

        [HttpGet("{id}")]
        public ActionResult<CustomerDto> Get(int id) 
        {
            try
            {
                return Ok(this.PersonService.findPersonById(id));
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ObjectNotFoundException))
                {
                    return NotFound(ex.Message);
                }
                return BadRequest(ex.Message);
            } 
        }

        [HttpPost]
        public ActionResult<CustomerDto> AddPerson(CustomerDto customer) 
        {
            try
            {
                return Ok(this.PersonService.CreatePerson(customer));
            }
            catch (Exception ex) 
            {
                if (ex.GetType() == typeof(ObjectAlreadyExistsException))
                {
                    return BadRequest(ex.Message);
                } 
                else if (ex.GetType() == typeof(ValidationException))
                {
                    return BadRequest(ex.Message);
                }
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<CustomerDto> UpdatePerson(CustomerDto customer) 
        {
            try
            {
                return Ok(this.PersonService.UpdatePerson(customer));
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ObjectNotFoundException))
                {
                    return NotFound(ex.Message);
                } 
                else if (ex.GetType() == typeof(ValidationException))
                {
                    return BadRequest(ex.Message);
                }
                return BadRequest(ex.Message);
            }
        }
    }
}
