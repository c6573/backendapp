﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Caravanstalling_WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase, IDocumentController
    {
        public readonly IDocumentService DocumentService;

        public DocumentController(IDocumentService DocumentService)
        {
            this.DocumentService = DocumentService;
        }

        [HttpPost("/upload-file")]
        public async Task<ActionResult> uploadFile(DocumentDto document)
        {
            try
            {
                await DocumentService.uploadDocument(document);
                return NoContent(); 
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> deleteDocument(int id) 
        {
            try
            {
                await DocumentService.deleteDocument(id);
                return NoContent();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DocumentDto>> downloadDocument(int id)
        {
            try
            {
                return Ok(await DocumentService.downloadDocument(id));
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
