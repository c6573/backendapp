﻿using Microsoft.AspNetCore.Mvc;

namespace Caravanstalling_WebAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase, INoteController
    {
        private readonly INoteService noteService;

        public NotesController(INoteService noteService)
        { 
            this.noteService = noteService;
        }

        [HttpPost("/save-note")]
        public NoteDto saveNote(NoteDto noteDto) 
        {
            return this.noteService.saveNotes(noteDto).Result;
        }
    }
}
