﻿namespace Caravanstalling_WebAPI.Models
{
    public class Document
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string uploadDate { get; set; }
        public string size { get; set; }
        public string fileName { get; set; }
        public string fileString { get; set; }
        public virtual Customer? Customers { get; set; }
        public virtual Item? item { get; set; }
    }
}
