﻿namespace Caravanstalling_WebAPI.Models
{
    public class Settings
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string type { get; set; }
        public string label { get; set; }
        public string value { get; set; }
    }
}
