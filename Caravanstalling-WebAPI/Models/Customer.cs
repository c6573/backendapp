﻿namespace Caravanstalling_WebAPI.Models
{
    public class Customer: Person
    {
        public string? Street { get; set; }
        public string? HouseNumber { get; set; }
        public string? PostalCode { get; set; }
        public string? City { get; set; }
        [Required]
        public string? PhoneNumber { get; set; }
        [Required]
        public string? Email { get; set; }
        
        [InverseProperty("Customer")]
        public virtual ICollection<Item>? Items { get; set; }
        
        [InverseProperty("SecondaryOwners")]
        public virtual ICollection<Item>? SecondaryOwnedItems { get; set; }
        public virtual ICollection<Document>? Documents { get; set; }
        public virtual Notes Notes { get; set; }
    }
}
