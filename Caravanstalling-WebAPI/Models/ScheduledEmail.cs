﻿namespace Caravanstalling_WebAPI.Models
{
    public class ScheduledEmail
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string emailAdress { get; set; }
        public string state { get; set; }
        public string emailType { get; set; }
        public int numReminded { get; set; }
        public DateTime remindAt { get; set; }

        public ScheduledEmail() 
        {
        }

        public ScheduledEmail(string emailAdress, string state, string emailType, int numReminded, DateTime remindAt) 
        {
            this.emailAdress = emailAdress;
            this.state = state;
            this.emailType = emailType;
            this.numReminded = numReminded;
            this.remindAt = remindAt;
        }
    }
}
