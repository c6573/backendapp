﻿namespace Caravanstalling_WebAPI.Models
{
    public class Notes
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string note { get; set; }
        public virtual int? customerId { get; set; }
        public virtual Customer? Customer { get; set; }
        public virtual int? itemId { get; set; }
        public virtual Item? item { get; set; }
    }
}
