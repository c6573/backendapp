﻿namespace Caravanstalling_WebAPI.Models
{
    public class Item
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? LicensePlateNumber { get; set; }
        [Required]
        public float Length { get; set; }
        public bool BikeCarrier { get; set; }
        [Required]
        public string type { get; set; }
        public int CustomerId { get; set; }
        [Required]
        public virtual Customer Customer { get; set; }
        [InverseProperty("SecondaryOwnedItems")]
        public virtual ICollection<Customer>? SecondaryOwners { get; set; }
        public virtual ICollection<Document>? Documents { get; set; }
        public virtual Notes Notes { get; set; }
    }
}
