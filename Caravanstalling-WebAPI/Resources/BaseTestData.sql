SET GLOBAL max_allowed_packet=1073741824;

insert into customer (FirstName, LastName, City, Email, HouseNumber, PhoneNumber, PostalCode, Street) 
values ("Frans", "Sjans", "Utrecht", "frans@gmail.com", "1", "06-12345678", "1234AB", "FranseStraat");
insert into customer (FirstName, LastName, City, Email, HouseNumber, PhoneNumber, PostalCode, Street) 
values ("Kees", "Kaas", "Utrecht", "Kees@gmail.com", "1", "06-12345678", "1234AB", "FranseStraat");
insert into customer (FirstName, LastName, City, Email, HouseNumber, PhoneNumber, PostalCode, Street) 
values ("Hans", "Anders", "Utrecht", "Hans@gmail.com", "1", "06-12345678", "1234AB", "FranseStraat");
insert into customer (FirstName, LastName, City, Email, HouseNumber, PhoneNumber, PostalCode, Street) 
values ("meneer", "invalid", "Utrecht", "dit is geen email toch", "1", "06-12345678", "1234AB", "FranseStraat");

insert into item (LicensePlateNumber, length, CustomerId, BikeCarrier, type) values ("123-asd-456", 6, 1, true, "caravan");
insert into item (LicensePlateNumber, length, CustomerId, BikeCarrier, type) values ("1-afa-123", 5, 1, false, "caravan");
insert into customeritem (SecondaryOwnedItemsId, SecondaryOwnersId) values (2,2);

insert into document (CustomersId, uploadDate, size, fileName, fileString) values (2, '01-01-01', '3.5MB', 'TestFile1.pdf', 'test string');
insert into document (CustomersId, uploadDate, size, fileName, fileString) values (2, '01-01-01', '3.5MB', 'TestFile2.pdf', 'test string');