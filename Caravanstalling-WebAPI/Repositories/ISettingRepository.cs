﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface ISettingRepository
    {
        public Task<List<Settings>> getSettingsByType(string type);
    }
}
