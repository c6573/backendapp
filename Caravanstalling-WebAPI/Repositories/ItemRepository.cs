﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class ItemRepository : IItemRepository
    {

        private readonly ApplicationDbContext applicationDbContext;
        public ItemRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }
        
        public async Task<Item> AddItem(Item item)
        {
            var customer = applicationDbContext.Customer.FirstOrDefault(c => c.Id == item.Customer.Id); 
            if (customer != null) applicationDbContext.Customer.Attach(customer);
            applicationDbContext.Item.Add(item);
            await applicationDbContext.SaveChangesAsync();

            return getSingleItem(item.Id).Result;
        }

        public async Task<List<Item>> getAllItems()
        {

            List<Item> items = await this.applicationDbContext.Item
                .Include(i => i.Documents)
                .Include(i => i.Notes)
                .Include(i => i.SecondaryOwners)
                .ToListAsync();

            return items;
        }

        public async Task<Item> GetItemById(int id)
        {
            return await getSingleItem(id);
        }

        public async Task<Item> UpdateItem(Item itemtoUpdate)
        {
            Item item = getSingleItem(itemtoUpdate.Id).Result;
            item.type = itemtoUpdate.type;
            item.BikeCarrier = itemtoUpdate.BikeCarrier;
            item.Length = itemtoUpdate.Length;
            item.CustomerId = itemtoUpdate.Customer.Id;
            item.LicensePlateNumber = itemtoUpdate.LicensePlateNumber;

            item.SecondaryOwners.Clear();

            foreach (Customer secondaryOwnerToAdd in itemtoUpdate.SecondaryOwners)
            {
                Customer secondaryOwner = applicationDbContext.Customer.FirstOrDefault(c => c.Id == secondaryOwnerToAdd.Id);
                if (secondaryOwner != null) item.SecondaryOwners.Add(secondaryOwner);
            }

            applicationDbContext.Update(item);
            await applicationDbContext.SaveChangesAsync();

            return getSingleItem(item.Id).Result;
        }

        public async Task DeleteItem(int id) 
        {
            var item = await applicationDbContext.Item.FindAsync(id);

            applicationDbContext.Item.Remove(item);

            await applicationDbContext.SaveChangesAsync();
        }

        private async Task<Item> getSingleItem(int id)
        {
            return await this.applicationDbContext.Item
               .Include(i => i.Documents)
               .Include(i => i.Notes)
               .Include(i => i.SecondaryOwners)
               .SingleOrDefaultAsync(i => i.Id == id);
        }
    }
}
