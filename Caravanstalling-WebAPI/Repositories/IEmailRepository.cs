﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface IEmailRepository
    {
        public Task scheduleEmail(ScheduledEmail email);
        public Task<List<ScheduledEmail>> GetScheduledEmails();
        public Task<ScheduledEmail> GetScheduledEmailByEmailTypeAndEmailAddress(string emailType, string email);
        public Task updateScheduledEmail(List<ScheduledEmail> scheduledEmails);
        public Task<ScheduledEmail> getScheduledEmailById(int id);
    }
}
