﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class EmailRepository : IEmailRepository
    {
        private readonly ApplicationDbContext applicationDbContext;
        public EmailRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task scheduleEmail(ScheduledEmail email)
        {
            this.applicationDbContext.ScheduledEmail.Add(email);
            await this.applicationDbContext.SaveChangesAsync();
        }

        public async Task<List<ScheduledEmail>> GetScheduledEmails() 
        {
            return await this.applicationDbContext.ScheduledEmail
                .ToListAsync();
        }

        public async Task<ScheduledEmail> getScheduledEmailById(int id) 
        {
            return await this.applicationDbContext.ScheduledEmail
                .Where(s => s.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<ScheduledEmail> GetScheduledEmailByEmailTypeAndEmailAddress(string emailType, string email)
        {
            return await this.applicationDbContext.ScheduledEmail
                .Where(s => s.emailType == emailType && s.emailAdress == email)
                .FirstOrDefaultAsync();
        }

        public async Task updateScheduledEmail(List<ScheduledEmail> scheduledEmails) 
        {
            this.applicationDbContext.ScheduledEmail.UpdateRange(scheduledEmails);
            await this.applicationDbContext.SaveChangesAsync();
        }
    }
}
