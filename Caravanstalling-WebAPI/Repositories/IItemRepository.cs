﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface IItemRepository
    {
        public Task<List<Item>> getAllItems();
        public Task<Item> GetItemById(int id);
        public Task<Item> AddItem(Item item);
        public Task<Item> UpdateItem(Item item);
        public Task DeleteItem(int id);
    }
}
