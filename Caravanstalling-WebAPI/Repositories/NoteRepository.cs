﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class NoteRepository: INoteRepository
    {
        private readonly ApplicationDbContext applicationDbContext;
        public NoteRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task<Notes> saveNoteForCustomer(Notes notes, int? customerId) 
        {
            Customer customer = this.applicationDbContext.Customer
                .Where(c => c.Id == customerId)
                .Include(c => c.Notes)
                .Include(c => c.Items)
                .Include(c => c.Documents)
                .FirstOrDefault();

            if (String.IsNullOrEmpty(customer.Notes?.note))
            {
                customer.Notes = notes;
            }
            else 
            {
                this.applicationDbContext.ChangeTracker.Clear();
                this.applicationDbContext.Notes.Update(notes);
            }
            await this.applicationDbContext.SaveChangesAsync();
            return await getNoteForCustomer(customerId);
        }

        public async Task<Notes> saveNoteforItem(Notes notes, int? itemId)
        {
            Item item = this.applicationDbContext.Item
                .Where(i => i.Id == itemId)
                .Include(i => i.Notes)
                .Include(i => i.Customer)
                .Include(i => i.Documents)
                .FirstOrDefault();

            if (String.IsNullOrEmpty(item.Notes?.note))
            {
                item.Notes = notes;
            }
            else
            {
                this.applicationDbContext.ChangeTracker.Clear();
                this.applicationDbContext.Notes.Update(notes);
            }

            await this.applicationDbContext.SaveChangesAsync();
            return await getNotesForItem(itemId);
        }

        public async Task<Notes> getNoteForCustomer(int? customerId) 
        {
            return await this.applicationDbContext.Notes
               .Where(n => n.customerId == customerId)
               .FirstOrDefaultAsync();
        }

        public async Task<Notes> getNotesForItem(int? itemId)
        {
            return await this.applicationDbContext.Notes
                .Where(n => n.itemId == itemId)
                .FirstOrDefaultAsync();
        }

        public async Task deleteNote(int id)
        {
            var note = await applicationDbContext.Notes
                .Where(n => n.Id == id)
                .FirstOrDefaultAsync();

            this.applicationDbContext.Notes.Remove(note);

            await applicationDbContext.SaveChangesAsync();
        }
    }
}
