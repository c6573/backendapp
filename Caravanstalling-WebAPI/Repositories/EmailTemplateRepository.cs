﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class EmailTemplateRepository: IEmailTemplateRepository
    {
        private readonly ApplicationDbContext applicationDbContext;
        public EmailTemplateRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task<List<EmailTemplate>> GetEmailTemplates()
        {
            List<EmailTemplate> emailTemplates = await this.applicationDbContext.EmailTemplate
                .ToListAsync();

            return emailTemplates;
        }

        public async Task<EmailTemplate> getEmailTemplateByType(string emailType)
        {
            return await this.applicationDbContext.EmailTemplate
                .Where(template => template.emailType == emailType)
                .FirstOrDefaultAsync();
        }

        public async Task UpdateTemplate(EmailTemplate emailTemplate)
        {
            this.applicationDbContext.EmailTemplate.Update(emailTemplate);
            await this.applicationDbContext.SaveChangesAsync();
        }
    }
}
