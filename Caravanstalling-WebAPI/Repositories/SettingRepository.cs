﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class SettingRepository: ISettingRepository
    {
        private readonly ApplicationDbContext applicationDbContext;

        public SettingRepository(ApplicationDbContext applicationDbContext) 
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task<List<Settings>> getSettingsByType(string type)
        {
            return await this.applicationDbContext.Settings
                .Where(setting => setting.type == type)
                .ToListAsync();
        }
    }
}
