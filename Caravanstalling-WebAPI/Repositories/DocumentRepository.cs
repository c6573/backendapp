﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class DocumentRepository : IDocumentRepository
    {
        private readonly ApplicationDbContext applicationDbContext;
        private readonly IPersonRepository personRepository;
       
        public DocumentRepository(ApplicationDbContext applicationDbContext, IPersonRepository personRepository)
        {
            this.applicationDbContext = applicationDbContext;
            this.personRepository = personRepository;
        }

        public async Task<List<Document>> getAllDocumentsForCustomer(int? customerId)
        {
            return await this.applicationDbContext.Document
                .Where(d => d.Customers.Id == customerId)
                .ToListAsync();
        }

        public async Task<List<Document>> getAllDocumentsForItem(int? itemId)
        {
            return await this.applicationDbContext.Document
                .Where(d => d.item.Id == itemId)
                .ToListAsync();
        }

        public async Task saveDocumentForCustomer(Document document, int? customerId)
        {
            Customer customer = this.applicationDbContext.Customer
                .Where(c => c.Id == customerId)
                .Include(c => c.Documents)
                .FirstOrDefault();

            customer.Documents.Add(document);
            await this.applicationDbContext.SaveChangesAsync();
        }

        public async Task saveDocumentForItem(Document document, int? itemId)
        {
            Item item = this.applicationDbContext.Item
                .Where(i => i.Id == itemId)
                .Include(i => i.Customer)
                .Include(i => i.Documents)
                .FirstOrDefault();

            item.Documents.Add(document);
            await this.applicationDbContext.SaveChangesAsync();
        }

        public async Task deleteDocument(int id)
        {
            Document documentToRemove = await this.applicationDbContext.Document.FindAsync(id);
            this.applicationDbContext.Document.Remove(documentToRemove);
            await this.applicationDbContext.SaveChangesAsync();
        }

        public async Task<Document> getDocumentById(int id) 
        {
            return await this.applicationDbContext.Document
                .Where(d => d.Id == id)
                .FirstOrDefaultAsync();
        }
    }
}
