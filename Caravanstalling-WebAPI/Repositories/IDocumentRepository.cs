﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface IDocumentRepository
    {
        public Task<List<Document>> getAllDocumentsForCustomer(int? customerId);
        public Task<List<Document>> getAllDocumentsForItem(int? itemId);
        public Task saveDocumentForCustomer(Document document, int? customerId);
        public Task saveDocumentForItem(Document document, int? itemId);
        public Task deleteDocument(int id);
        public Task<Document> getDocumentById(int id);
    }
}
