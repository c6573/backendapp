﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface INoteRepository
    {
        public Task<Notes> saveNoteForCustomer(Notes notes, int? customerId);
        public Task<Notes> saveNoteforItem(Notes notes, int? itemId);
        public Task<Notes> getNoteForCustomer(int? customerId);
        public Task<Notes> getNotesForItem(int? itemId);
        public Task deleteNote(int id);
    }
}
