﻿namespace Caravanstalling_WebAPI.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly ApplicationDbContext applicationDbContext;

        public PersonRepository(ApplicationDbContext applicationDbContext) 
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task<List<Customer>> getAllPersons() 
        {
            List<Customer> customers = await this.applicationDbContext.Customer
                .Include(c => c.Documents)
                .Include(c => c.Notes)
                .Include(c => c.Items)
                .ThenInclude(SecondaryOwner => SecondaryOwner.SecondaryOwners)
                .Include(c => c.Items)
                .ThenInclude(Notes => Notes.Notes)
                .ToListAsync();
            return customers;
        }

        public Customer getPersonById(int id)
        {
            return getSingleCustomer(id).Result;
        }

        public async Task<Customer> AddCustomer(Customer customer)
        {
            applicationDbContext.Customer.Add(customer);
            await applicationDbContext.SaveChangesAsync();

            return getSingleCustomer(customer.Id).Result;
        }

        public async Task<Customer> UpdateCustomer(Customer customerToUpdate)
        {
            var customer = getSingleCustomer(customerToUpdate.Id).Result;
            customer.Items.Clear();
            customer.Email = customerToUpdate.Email;
            customer.FirstName = customerToUpdate.FirstName;
            customer.LastName = customerToUpdate.LastName;
            customer.PhoneNumber = customerToUpdate.PhoneNumber;
            customer.HouseNumber = customerToUpdate.HouseNumber;
            customer.City = customerToUpdate.City;
            customer.PostalCode = customerToUpdate.PostalCode;
            customer.Street = customerToUpdate.Street;

            foreach (Item itemToLink in customerToUpdate.Items)
            {
                var item = applicationDbContext.Item.FirstOrDefault(i => i.Id == itemToLink.Id);
                if (item != null) customer.Items.Add(item);
            }

            applicationDbContext.Customer.Update(customer);
            await applicationDbContext.SaveChangesAsync();

            return getSingleCustomer(customer.Id).Result;
        }

        private async Task<Customer> getSingleCustomer(int id) 
        {
            return await applicationDbContext.Customer
                   .Include(c => c.Items)
                   .ThenInclude(SecondaryOwner => SecondaryOwner.SecondaryOwners)
                   .SingleOrDefaultAsync(c => c.Id == id);
        }

        public async Task<Customer> getCustomerByEmailAdress(string emailAdress)
        {
            return await applicationDbContext.Customer
                .Where(c => c.Email == emailAdress)
                .FirstOrDefaultAsync();
        }
    }
}
