﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface IEmailTemplateRepository
    {
        public Task<List<EmailTemplate>> GetEmailTemplates();
        public Task<EmailTemplate> getEmailTemplateByType(string emailType);
        public Task UpdateTemplate(EmailTemplate emailTemplate);
    }
}
