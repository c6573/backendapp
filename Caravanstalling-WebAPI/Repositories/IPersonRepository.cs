﻿namespace Caravanstalling_WebAPI.Repositories
{
    public interface IPersonRepository
    {
        public Task<List<Customer>> getAllPersons();
        public Customer getPersonById(int id);
        public Task<Customer> AddCustomer(Customer customer);
        public Task<Customer> UpdateCustomer(Customer person);
        public Task<Customer> getCustomerByEmailAdress(string emailAdress);
    }
}
