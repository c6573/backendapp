﻿namespace Caravanstalling_WebAPI.Cron.Models
{
    public class JobMetadata
    {
        public Guid JobId { get; set; }
        public Type JobType { get; set; }
        public string JobName { get; set; }
        public string CronExpression { get; set; }

        public JobMetadata(Guid Id, Type JobType, string JobName,
            string CronExpression)
        { 
            this.JobId = Id;
            this.JobType = JobType;
            this.JobName = JobName;
            this.CronExpression = CronExpression;
        }
    }
}
