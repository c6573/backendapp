﻿using Caravanstalling_WebAPI.Cron.Models;
using Quartz;
using Quartz.Spi;

namespace Caravanstalling_WebAPI.Cron.Scheduler
{
    public class EmailScheduler : IHostedService
    {

        public IScheduler scheduler { get; set; }
        private readonly IJobFactory jobFactory;
        private readonly List<JobMetadata> jobMetaDatas;
        private readonly ISchedulerFactory schedulerFactory;

        public EmailScheduler(IJobFactory jobFactory, List<JobMetadata> jobMetadatas, ISchedulerFactory schedulerFactory)
        {
            this.jobFactory = jobFactory;
            this.jobMetaDatas = jobMetadatas;
            this.schedulerFactory = schedulerFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            scheduler = await schedulerFactory.GetScheduler();
            scheduler.JobFactory = jobFactory;

            jobMetaDatas?.ForEach(jobMetaData =>
            {
                // create job
                IJobDetail jobDetail = CreateJob(jobMetaData);
                // create trigger
                ITrigger trigger = CreateTrigger(jobMetaData);
                // schedule job
                scheduler.ScheduleJob(jobDetail, trigger, cancellationToken).GetAwaiter();
            });
            // start the scheduler
            await scheduler.Start(cancellationToken);
        }

        private ITrigger CreateTrigger(JobMetadata jobMetaData)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity(jobMetaData.JobId.ToString())
                .WithCronSchedule(jobMetaData.CronExpression)
                .WithDescription(jobMetaData.JobName)
                .Build();
        }

        private IJobDetail CreateJob(JobMetadata jobMetaData)
        {
            return JobBuilder
                .Create(jobMetaData.JobType)
                .WithIdentity(jobMetaData.JobId.ToString())
                .WithDescription(jobMetaData.JobName)
                .Build();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await scheduler.Shutdown();
        }
    }
}
