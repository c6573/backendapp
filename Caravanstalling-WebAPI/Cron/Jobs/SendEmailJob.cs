﻿using Quartz;

namespace Caravanstalling_WebAPI.Cron.Jobs
{
    [DisallowConcurrentExecution]
    public class SendEmailJob : IJob
    {
        private readonly IEmailService emailService;

        public SendEmailJob(IEmailService emailService)
        {
            this.emailService = emailService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await this.emailService.sendScheduledEmail();
        }
    }
}
