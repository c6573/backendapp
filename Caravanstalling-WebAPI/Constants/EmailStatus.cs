﻿namespace Caravanstalling_WebAPI.Constants
{
    public static class EmailStatus
    {
        public static readonly string Pending = "pending";
        public static readonly string Complete = "complete";
        public static readonly string Send = "send";
    }
}
