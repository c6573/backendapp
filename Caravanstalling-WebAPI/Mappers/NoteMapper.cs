﻿namespace Caravanstalling_WebAPI.Mappers
{
    public class NoteMapper : INoteMapper
    {
        public Mapper toDto()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Notes, NoteDto>();
            });

            return new Mapper(config);
        }

        public Mapper toNote()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NoteDto, Notes>();
            });

            return new Mapper(config);
        }
    }
}
