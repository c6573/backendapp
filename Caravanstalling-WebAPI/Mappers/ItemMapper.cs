﻿namespace Caravanstalling_WebAPI.Mappers
{
    public class ItemMapper : IItemMapper
    {
        public readonly ICustomerMapper customerMapper;
        public readonly IPersonService personService;
        public readonly IDocumentMapper documentMapper;

        public ItemMapper(ICustomerMapper customerMapper, IPersonService personService, IDocumentMapper documentMapper) 
        {
            this.customerMapper = customerMapper;
            this.personService = personService;
            this.documentMapper = documentMapper;
        }

        public ItemDto toDto(Item item)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Item, ItemDto>()
                .ForMember(i => i.SecondaryOwners, so => so.Ignore())
                .ForMember(i => i.Documents, d => d.Ignore());
                cfg.CreateMap<Notes, NoteDto>();
            });

            Mapper mapper = new(config);
            ItemDto itemDto = new();
            itemDto = mapper.Map<ItemDto>(item);
            itemDto.Documents = new List<DocumentDto>();

            itemDto.Owner = personService.findPersonByIdWithoutItems(item.CustomerId);
            foreach (var document in item.Documents)
            {
                itemDto.Documents.Add(this.documentMapper.ToDto(document));
            }

            if (item.SecondaryOwners?.Count > 0)
            {
                foreach (var secondaryOwner in item.SecondaryOwners)
                {
                    itemDto.SecondaryOwners?.Add(this.customerMapper.ToDto()
                        .Map<CustomerDto>(secondaryOwner));
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("No secondary person was found");
            }
            return itemDto;
        }

        public Item toItem(ItemDto dto) 
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ItemDto, Item>()
                       .ForMember(i => i.Customer, opt => opt.MapFrom(src => src.Owner))
                       .ForMember(i => i.SecondaryOwners, opt => opt.MapFrom(src => src.SecondaryOwners));
                cfg.CreateMap<CustomerDto, Customer>();
                cfg.CreateMap<DocumentDto, Document>();
                cfg.CreateMap<NoteDto, Notes>();
            });

            Mapper mapper = new(config);
            Item item = new();
            item = mapper.Map<Item>(dto);

            if (dto.SecondaryOwners.Count > 0) {
                for(int i = 0; i < dto.SecondaryOwners.Count; i++)
                {
                    item.SecondaryOwners.ToList()[i].Id = dto.SecondaryOwners[i].Id;
                }
            }    

            return item;
        }
    }
}
