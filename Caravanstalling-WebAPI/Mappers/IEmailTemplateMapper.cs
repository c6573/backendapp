﻿namespace Caravanstalling_WebAPI.Mappers
{
    public interface IEmailTemplateMapper
    {
        public Mapper toDto();
        public Mapper toEmailTemplate();
    }
}
