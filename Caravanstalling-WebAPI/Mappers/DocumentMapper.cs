﻿namespace Caravanstalling_WebAPI.Mappers
{
    public class DocumentMapper : IDocumentMapper
    {
        private string[] suffixes = { "Bytes", "KB", "MB", "GB", "TB", "PB" };

        MapperConfiguration DocumentConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<DocumentDto, Document>();
        });

        MapperConfiguration DocumentDtoConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Document, DocumentDto>().ForMember(d => d.fileString, s => s.Ignore());
        });

        MapperConfiguration DocumentDtoWithFileStringConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Document, DocumentDto>();
        });

        public Document ToDocument(DocumentDto documentDto)
        {
            Mapper mapper = new Mapper(DocumentConfig);
            Document document = new Document();
            document = mapper.Map<Document>(documentDto);

            byte[] bytes = Convert.FromBase64String(document.fileString);
            MemoryStream stream = new MemoryStream(bytes);

            IFormFile formFile = new FormFile(stream, 0, bytes.Length, document.fileName, document.fileName)
            {
                Headers = new HeaderDictionary(),
                ContentType = "application/pdf"
            };

            using (var ms = new MemoryStream())
            {
                formFile.CopyTo(ms);
                var fileBytes = ms.ToArray();
                document.size = this.formatFileSize(fileBytes.Length);
            }

            return document;
        }

        public DocumentDto ToDto(Document document)
        {
            DocumentDto dto = new DocumentDto();
            dto = new Mapper(DocumentDtoConfig).Map<DocumentDto>(document);
            return dto;
        }

        public DocumentDto ToDtoWithfileString(Document document)
        {
            DocumentDto dto = new DocumentDto();
            dto = new Mapper(DocumentDtoWithFileStringConfig).Map<DocumentDto>(document);
            return dto;
        }

        private string formatFileSize(Int64 bytes) 
        {
            int counter = 0;
            decimal number = (decimal)bytes;
            while (Math.Round(number / 1024) >= 1)
            {
                number = number / 1024;
                counter++;
            }
            return string.Format("{0:n1}{1}", number, suffixes[counter]);
        }
    }
}
