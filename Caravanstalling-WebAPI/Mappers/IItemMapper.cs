﻿namespace Caravanstalling_WebAPI.Mappers
{
    public interface IItemMapper
    {
        public ItemDto toDto(Item item);
        public Item toItem(ItemDto dto);
    }
}
