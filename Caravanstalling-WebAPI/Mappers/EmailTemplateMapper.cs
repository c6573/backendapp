﻿namespace Caravanstalling_WebAPI.Mappers
{
    public class EmailTemplateMapper : IEmailTemplateMapper
    {
        public Mapper toDto()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<EmailTemplate, EmailTemplateDto>();
            });

            return new Mapper(config);
        }

        public Mapper toEmailTemplate()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<EmailTemplateDto, EmailTemplate>();
            });

            return new Mapper(config);
        }
    }
}
