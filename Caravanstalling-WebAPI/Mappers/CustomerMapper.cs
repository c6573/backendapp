﻿namespace Caravanstalling_WebAPI.Mappers
{
    public class CustomerMapper : ICustomerMapper
    {
        private readonly IDocumentMapper documentMapper;
        public CustomerMapper(IDocumentMapper documentMapper)
        { 
            this.documentMapper = documentMapper;
        }

        MapperConfiguration CustomerAndObjectConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Customer, CustomerDto>().ForMember(c => c.Documents, d => d.Ignore());
            cfg.CreateMap<Item, ItemDto>().ForMember(o => o.SecondaryOwners, so => so.Ignore());
            cfg.CreateMap<Notes, NoteDto>();
            cfg.CreateMap<Document, DocumentDto>();
        });

        MapperConfiguration CustomerConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Customer, CustomerDto>().ForMember(c => c.Items, o => o.Ignore());
            cfg.CreateMap<Notes, NoteDto>();
            cfg.CreateMap<Document, DocumentDto>();
        });

        public Mapper ToDto()
        {
            return new Mapper(CustomerConfig);
        }

        public CustomerDto ToDtoWithObjects(Customer customer)
        {
            Mapper mapper = new Mapper(CustomerAndObjectConfig);
            CustomerDto customerDto = new CustomerDto();
            customerDto = mapper.Map<CustomerDto>(customer);
            customerDto.Documents = new List<DocumentDto>();

            if (customer.Documents != null)
            {
                foreach (var document in customer.Documents)
                {
                    customerDto.Documents.Add(this.documentMapper.ToDto(document));
                }
            }
 
            if (customer.Items?.Count > 0)
            {
                foreach (var vehicle in customer.Items)
                {
                    if (vehicle.SecondaryOwners?.Count > 0)
                    {
                        foreach (var secondaryOwner in vehicle.SecondaryOwners)
                        {
                            customerDto
                                .Items?.Find(objectToFind => objectToFind.Id == vehicle.Id)?
                                .SecondaryOwners?.Add(this.ToDto()
                                .Map<CustomerDto>(secondaryOwner));
                        }
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("No secondary person was found");
                    }
                }
            }
            return customerDto;
        }

        public Mapper toCustomer() 
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CustomerDto, Customer>();
                cfg.CreateMap<ItemDto, Item>();
                cfg.CreateMap<DocumentDto, Document>();
                cfg.CreateMap<NoteDto, Notes>();
            });
            return new Mapper(config);
        }
    }
}
