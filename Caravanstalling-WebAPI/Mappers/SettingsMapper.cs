﻿namespace Caravanstalling_WebAPI.Mappers
{
    public class SettingsMapper : ISettingsMapper
    {
        public Mapper toDto()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Settings, SettingsDto>();
            });

            return new Mapper(config);
        }

        public Mapper toSetting()
        {
            throw new NotImplementedException();
        }
    }
}
