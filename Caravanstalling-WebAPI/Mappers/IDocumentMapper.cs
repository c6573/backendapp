﻿namespace Caravanstalling_WebAPI.Mappers
{
    public interface IDocumentMapper
    {
        public DocumentDto ToDto(Document document);
        public Document ToDocument(DocumentDto documentDto);
        public DocumentDto ToDtoWithfileString(Document document);
    }
}
