﻿namespace Caravanstalling_WebAPI.Mappers
{
    public interface INoteMapper
    {
        public Mapper toDto();
        public Mapper toNote();
    }
}
