﻿namespace Caravanstalling_WebAPI.Mappers
{
    public interface ICustomerMapper
    {

        public Mapper ToDto();
        public CustomerDto ToDtoWithObjects(Customer customer);
        public Mapper toCustomer();
    }
}
