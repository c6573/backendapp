﻿namespace Caravanstalling_WebAPI.Mappers
{
    public interface ISettingsMapper
    {
        public Mapper toDto();
        public Mapper toSetting();
    }
}
