﻿namespace Caravanstalling_WebAPI.Helpers
{
    public interface IEmailHelper
    {
        public Boolean checkIfEmailIsvalid(string email);
    }
}
