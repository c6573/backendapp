FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
#EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Caravanstalling-WebAPI/Caravanstalling-WebAPI.csproj", "Caravanstalling-WebAPI/"]
RUN dotnet restore "Caravanstalling-WebAPI/Caravanstalling-WebAPI.csproj"
COPY . .
WORKDIR "/src/Caravanstalling-WebAPI"
RUN dotnet build "Caravanstalling-WebAPI.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Caravanstalling-WebAPI.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Caravanstalling-WebAPI.dll"]

